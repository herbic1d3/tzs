Vagrant.configure(2) do |config|

    VAGRANT_ROOT = "/vagrant"

    config.vm.box = "ubuntu/bionic64"
    config.vm.box_check_update = false
    config.vm.hostname = "tezos-node"
    config.disksize.size = '20GB'

    config.vm.provider "virtualbox" do |vb|
        vb.gui = false
        vb.cpus = 2
        vb.memory = "4096"
        vb.check_guest_additions = false
    end

    config.vm.network "forwarded_port", guest: 9852, host_ip: "127.0.0.1", host: 9852

    config.vm.synced_folder ".", "/vagrant", disabled: true
    config.vm.synced_folder ".", "/node",
        type: "rsync",
        rsync__exclude: [".git/", ".gitignore", ".idea/", ".vagrant/", "*.log", "venv/"]

    config.vm.provision "shell", inline: <<-SHELL
    export VAGRANT_ROOT="#{VAGRANT_ROOT}"
    [[ -f /home/vagrant/.bashrc ]] && sed -i '/force_color_prompt/s/^#//g' /home/vagrant/.bashrc
    /bin/bash -c /node/build.bash
  SHELL

end
