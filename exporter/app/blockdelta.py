import json
import requests
import time

from datetime import datetime

from prometheus_client.core import GaugeMetricFamily, REGISTRY
from prometheus_client import start_http_server


class BlockDelta(object):

    def __init__(self):
        self.url = "http://localhost:8732/monitor/bootstrapped"

    def collect(self):
        try:
            response = requests.get(url=self.url, verify=False, stream=True, timeout=30)
        except requests.Timeout:
            pass
        except requests.ConnectionError:
            pass

        for line in response.iter_lines(1):
            data = json.loads(line)

            metric = GaugeMetricFamily("current_block", "Current block delta time",
                                        labels=["block", "deltatime"])
            metric.add_metric([data["block"]], (datetime.now()-datetime.strptime(data["timestamp"], '%Y-%m-%dT%H:%M:%SZ')).total_seconds())
            break
        yield metric


if __name__ == "__main__":
    start_http_server(9852)
    REGISTRY.register(BlockDelta())
    while True:
        time.sleep(1)
