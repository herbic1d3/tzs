#!/bin/bash

# Объявление переменных
export IPT4="/sbin/iptables"
export IPT6="/sbin/ip6tables"
export SSHD=$(awk -F ' ' '/Port\ / { print $2 }' /etc/ssh/sshd_config)

IPT()
{
  $IPT4 "$@"
  $IPT6 "$@"
}

# Интерфейс который смотрит в интернет
export WAN=$(awk '$2 == "00000000" {print $1}' /proc/net/route)

# Очистка всех цепочек iptables
IPT -F
IPT -F -t nat
IPT -F -t mangle
IPT -X
IPT -t nat -X
IPT -t mangle -X

# Закрываем изначально ВСЁ (т.е. изначально все что не разрешено - запрещено):
IPT -P INPUT DROP
IPT -P OUTPUT DROP
IPT -P FORWARD DROP

# разрешаем локальный траффик для loopback и внутренней сети
for IFACE in $(ls /sys/class/net/) ; do
    if [ "$WAN" == "$IFACE" ]; then
        continue
    fi

    IPT -A INPUT -i $IFACE -j ACCEPT
    IPT -A OUTPUT -o $IFACE -j ACCEPT
    IPT -A FORWARD -i $IFACE -j ACCEPT
    IPT -A FORWARD -o $IFACE -j ACCEPT

    IPT -A FORWARD -i $IFACE -o $WAN -j ACCEPT
    IPT -A FORWARD -i $WAN -o $IFACE -j REJECT
done

# Состояние ESTABLISHED говорит о том, что это не первый пакет в соединении.
# Пропускать все уже инициированные соединения, а также дочерние от них
IPT -A INPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
# Пропускать новые, а так же уже инициированные и их дочерние соединения
IPT -A OUTPUT -p all -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
# Разрешить форвардинг для новых, а так же уже инициированных
# и их дочерних соединений
IPT -A FORWARD -p all -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
# Включаем фрагментацию пакетов. Необходимо из за разных значений MTU
IPT -I FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

# препятствовать спуфингу от нашего имени
IPT -I INPUT -m conntrack --ctstate NEW,INVALID -p tcp --tcp-flags SYN,ACK SYN,ACK -j REJECT --reject-with tcp-reset

# Отбрасывать все пакеты, которые не могут быть идентифицированы
# и поэтому не могут иметь определенного статуса.
IPT -A INPUT -m state --state INVALID -j DROP
IPT -A FORWARD -m state --state INVALID -j DROP

# block common attacks
IPT -A INPUT -p tcp ! --syn -m state --state NEW -j DROP
IPT -A OUTPUT -p tcp ! --syn -m state --state NEW -j DROP
IPT -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
IPT -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
IPT -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
IPT -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP

### Блокирование пакетов с неверными TCP флагами
IPT -A INPUT -i $WAN -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags FIN,ACK FIN -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags ACK,URG URG -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags ACK,FIN FIN -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags ACK,PSH PSH -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags ALL ALL -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags ALL NONE -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP
IPT -A INPUT -i $WAN -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP

# Port scanning protection
IPT -N port-scanning
IPT -A port-scanning -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s --limit-burst 2 -j RETURN
IPT -A port-scanning -j DROP

# **********************************************************************
# открытие портов извне:
# **********************************************************************
# Открываем порт для ssh
IPT -A INPUT -i $WAN -p tcp --dport $SSHD -j ACCEPT

# open port for prometheus exporter
IPT -A INPUT -i $WAN -p tcp --dport 9852 -j ACCEPT
IPT -A INPUT -i $WAN -p tcp --syn --dport 9852 -m connlimit --connlimit-above 3 -j REJECT

# **********************************************************************
# restart fail2ban
[[ `service --status-all | grep -c "fail2ban"` -eq 1 ]] && /bin/systemctl restart fail2ban.service

